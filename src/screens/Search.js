import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../actions";
import Home from "../containers/Home";
import { get } from "../utils";

class Search extends Home {
  static navigationOptions = props => ({
    title: get(props, ["navigation", "state", "params", "Search"], "")
  });

  componentWillMount() {
    return this.props.fetchCardData(
      get(this, ["props", "navigation", "state", "params", "Search"], "")
    );
  }
}

const mapStateToProps = (state, props) => {
  return Object.assign(
    {
      fetched: state.cards.fetched,
      cards: get(
        state,
        [
          "cards",
          get(props, ["navigation", "state", "params", "Search"], ""),
          "data"
        ],
        []
      )
    },
    props
  );
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(Actions, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(Search);
