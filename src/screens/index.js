import { StackNavigator } from "react-navigation";
import Home from "./Home";
import Card from "./Card";
import Search from "./Search";

const AppNavigator = StackNavigator(
  {
    Home: { screen: Home },
    Search: { screen: Search },
    Card: { screen: Card }
  },
  {
    initialRouteName: "Home"
  }
);

export default AppNavigator;
