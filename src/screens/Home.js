import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as Actions from "../actions";
import Home from "../containers/Home";

class HomeScreen extends Home {
  static navigationOptions = {
    title: "All Cards"
  };

  componentWillMount() {
    return this.props.fetchCardsData();
  }
}

const mapStateToProps = (state, props) => {
  return Object.assign(
    {
      fetched: state.cards.fetched,
      cards: state.cards.data
    },
    props
  );
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(Actions, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
