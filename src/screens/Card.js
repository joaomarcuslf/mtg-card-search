import Card from "../containers/Card";
import { get } from "../utils";

export default class DetailsScreen extends Card {
  static navigationOptions = props => ({
    title: get(props, ["navigation", "state", "params", "name"], "")
  });
}
