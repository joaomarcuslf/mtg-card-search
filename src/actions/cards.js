import { getData } from "../utils";

export const LOAD_CARDS_REQUEST = "LOAD_CARDS_REQUEST";
export const LOAD_CARDS_SUCCESS = "LOAD_CARDS_SUCCESS";
export const LOAD_CARDS_ERROR = "LOAD_CARDS_ERROR";

const mtgUrl = "https://api.magicthegathering.io/v1/cards";

export const fetchCardsData = () => {
  return dispatch => {
    dispatch({ type: LOAD_CARDS_REQUEST });

    getData(mtgUrl)
      .then(response => response.json())
      .then(data => {
        dispatch({
          type: LOAD_CARDS_SUCCESS,
          data: {
            data: data.cards
          }
        });
      });
  };
};

export const LOAD_CARD_REQUEST = "LOAD_CARD_REQUEST";
export const LOAD_CARD_SUCCESS = "LOAD_CARD_SUCCESS";
export const LOAD_CARD_ERROR = "LOAD_CARD_ERROR";

export const fetchCardData = name => {
  return dispatch => {
    dispatch({ type: LOAD_CARD_REQUEST, name });

    getData(`${mtgUrl}/?name=${name}`)
      .then(response => response.json())
      .then(data => {
        dispatch({
          type: LOAD_CARD_SUCCESS,
          data: {
            [name]: {
              data: data.cards
            }
          }
        });
      });
  };
};
