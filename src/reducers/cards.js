import {
  LOAD_CARDS_REQUEST,
  LOAD_CARDS_SUCCESS,
  LOAD_CARD_REQUEST,
  LOAD_CARD_SUCCESS
} from "../actions";

const initialState = { data: [], fetched: false };

const cards = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_CARDS_REQUEST:
    case LOAD_CARD_REQUEST:
      return Object.assign({}, state, { fetched: true });
    case LOAD_CARDS_SUCCESS:
    case LOAD_CARD_SUCCESS:
      return Object.assign({}, state, action.data);
    default:
      return state;
  }
};

export default cards;
