import { combineReducers } from "redux";

import cards from "./cards";
import page from "./page";
import navigation from "./navigation";

const rootReducer = combineReducers({
  page,
  navigation,
  cards
});

export default rootReducer;
