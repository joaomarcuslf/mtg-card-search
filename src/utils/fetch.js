export const getData = (url, headers = {}) => {
  return fetch(
    url,
    Object.assign(
      {
        cache: "no-cache",
        credentials: "same-origin",
        method: "GET",
        mode: "cors",
        redirect: "follow",
        referrer: "no-referrer"
      },
      headers
    )
  );
};
