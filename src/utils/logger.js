const logger = (type, msg) => console.log(`${type}:`, msg);

export default logger;
