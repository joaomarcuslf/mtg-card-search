import { createStore, applyMiddleware } from "redux";
import { createReactNavigationReduxMiddleware } from "react-navigation-redux-helpers";
import thunk from "redux-thunk";

import reducers from "./reducers";

const middleware = createReactNavigationReduxMiddleware(
  "root",
  state => state.nav
);

export default createStore(reducers, applyMiddleware(thunk, middleware));
