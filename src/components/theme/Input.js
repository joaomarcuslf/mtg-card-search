import styled from "styled-components";
import { TextInput } from "react-native";

export default styled(TextInput)`
  height: 40px;
  border-color: gray;
  border-width: 1px;
  margin: 5px;
  padding: 5px;
  flex: 2;
`;
