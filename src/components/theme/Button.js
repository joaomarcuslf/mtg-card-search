import styled from "styled-components";
import { Button } from "react-native";

export default styled(Button)`
  flex: 1;
`;
