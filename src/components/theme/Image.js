import React from "react";
import styled from "styled-components";
import { Image } from "react-native";
import ImagePlaceholder from "./ImagePlaceholder";

class StyledImage extends React.Component {
  render() {
    return this.props.placeHolder ? (
      <ImagePlaceholder {...this.props} />
    ) : (
      <Image {...this.props} />
    );
  }
}

export default styled(StyledImage)`
  width: 223px;
  height: 311px;
`;
