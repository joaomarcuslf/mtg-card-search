import styled from "styled-components";
import { View } from "react-native";

export default styled(View)`
  align-items: center;
  justify-content: center;
  margin: 5px;
  flex-direction: ${props => (props.direction ? props.direction : "column")};
`;
