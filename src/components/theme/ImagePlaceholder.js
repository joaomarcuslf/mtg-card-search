import React from "react";
import { Animated, View } from "react-native";

class ImagePlaceholder extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      thumbnailOpacity: new Animated.Value(0)
    };
  }

  fadeOut() {
    Animated.timing(this.state.thumbnailOpacity, {
      toValue: 0,
      duration: 250
    }).start();
  }

  fadeIn() {
    Animated.timing(this.state.thumbnailOpacity, {
      toValue: 1,
      duration: 10
    }).start();
  }

  render() {
    return (
      <View
        width={this.props.style.width}
        height={this.props.style.height}
        backgroundColor={"transparent"}
      >
        <Animated.Image
          resizeMode={"contain"}
          key={this.props.key}
          style={[
            {
              opacity: this.state.thumbnailOpacity,
              position: "absolute"
            },
            this.props.style
          ]}
          source={require("../../../statics/img/placeholder-image.png")}
          onLoad={event => this.fadeIn(event)}
        />

        <Animated.Image
          resizeMode={"contain"}
          key={this.props.key}
          style={this.props.style}
          source={this.props.source}
          onLoad={event => this.fadeOut(event)}
        />
      </View>
    );
  }
}

export default ImagePlaceholder;
