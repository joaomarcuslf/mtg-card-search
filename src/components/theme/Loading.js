import React from "react";
import { ActivityIndicator } from "react-native";
import styled from "styled-components";

class Loading extends React.Component {
  render() {
    return <ActivityIndicator size="large" color="#0000ff" {...this.props} />;
  }
}

export default styled(Loading)`
  flex: 1;
`;
