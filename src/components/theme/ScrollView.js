import styled from "styled-components";
import { ScrollView } from "react-native";

export default styled(ScrollView)`
  flex: 1;
  margin: 5px;
  padding: 5px;
`;
