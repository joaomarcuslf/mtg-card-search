import React from "react";
import styled from "styled-components";
import { Text } from "react-native";

class Paragraph extends React.Component {
  render() {
    return <Text {...this.props}>{this.props.children}</Text>;
  }
}

export default styled(Paragraph)`
  line-height: 20px;
  font-size: ${props => props.fontSize || "15px"};
  font-weight: ${props => (props.title ? "bold" : "normal")};
`;
