import styled from "styled-components";
import { View } from "react-native";

export default styled(View)`
  flex-direction: row;
  align-items: center;
  justify-content: center;
  margin: 5px;
  padding: 5px;
`;
