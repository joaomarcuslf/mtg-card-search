import React from "react";
import CardsList from "./CardsList";
import { get } from "../utils";
import {
  ScrollView,
  Container,
  RowForm,
  Input,
  Button,
  Loading
} from "./theme";

const cardsPadding = 311 * 8;

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text: get(
        this,
        ["props", "navigation", "state", "params", "cardName"],
        ""
      ),
      page: 1
    };

    this.onInputChange = this.onInputChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onInputChange(text) {
    this.setState({ text });
  }

  onSubmit() {
    if (this.state.text) return this.props.routeChange(this.state.text);
  }

  isCloseToBottom(paddingToBottom) {
    return ({ layoutMeasurement, contentOffset, contentSize }) =>
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom;
  }

  loadMoreCards() {
    this.setState({
      page: this.state.page + 1
    });
  }

  getCardsPage() {
    return get(this, ["props", "data"], []).slice(
      0,
      10 * (this.state.page + 1)
    );
  }

  render() {
    const { isLoading } = this.props;

    return (
      <ScrollView
        onScroll={({ nativeEvent }) => {
          if (this.isCloseToBottom(cardsPadding)(nativeEvent)) {
            this.loadMoreCards();
          }
        }}
        scrollEventThrottle={700}
      >
        <RowForm>
          <Input
            placeholder="Type Card Name"
            onChangeText={this.onInputChange}
            value={this.state.text}
          />
          <Button title="Ok" onPress={this.onSubmit} />
        </RowForm>

        <Container>
          {isLoading ? <Loading /> : <CardsList cards={this.getCardsPage()} />}
        </Container>
      </ScrollView>
    );
  }
}

export default Home;
