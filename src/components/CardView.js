import React from "react";
import { TouchableOpacity } from "react-native";
import { Image, Container } from "./theme";

export class CardView extends React.Component {
  render() {
    const { imageUrl, multiverseId } = this.props;

    return (
      <Container>
        <Image {...this.props} key={multiverseId} source={{ uri: imageUrl }} />
      </Container>
    );
  }
}

class CardViewTouchable extends React.Component {
  render() {
    const { name, routeChange } = this.props;

    return (
      <TouchableOpacity onPress={() => routeChange(name, this.props)}>
        <CardView placeHolder {...this.props} />
      </TouchableOpacity>
    );
  }
}

export default CardViewTouchable;
