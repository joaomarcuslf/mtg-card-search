import React from "react";
import { CardView } from "./CardView";
import { ScrollView, Container, Paragraph } from "./theme";

class Card extends React.Component {
  render() {
    const props = this.props.card;
    return (
      <ScrollView>
        <CardView placeHolder {...props} />

        <Container>
          <Paragraph title fontSize="19px">
            {props.name} - {props.manaCost}
          </Paragraph>
          <Paragraph>{props.type}</Paragraph>
          <Paragraph>{props.setName}</Paragraph>
          <Paragraph>{props.text}</Paragraph>
          <Paragraph>{props.flavor}</Paragraph>
        </Container>
      </ScrollView>
    );
  }
}

export default Card;
