import React from "react";
import { View } from "react-native";
import CardView from "../containers/CardView";

class Home extends React.Component {
  render() {
    return (
      <View>
        {this.props.cards.map((card, index) => (
          <CardView
            key={`${card.multiverseid}-${card.releaseDate}-${card.id}`}
            index={index}
            {...card}
          />
        ))}
      </View>
    );
  }
}

export default Home;
