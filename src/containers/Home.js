import Home from "../components/Home";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { NavigationActions } from "react-navigation";
import * as Actions from "../actions";

const mapStateToProps = (state, props) => {
  return Object.assign(
    {},
    {
      data: props.cards,
      isLoading: !state.cards.fetched || state.page.loading,
      routeChange: dispatch => Search =>
        dispatch(
          NavigationActions.navigate({
            routeName: "Search",
            params: { Search }
          })
        )
    },
    props
  );
};

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators(Actions, dispatch),
    dispatch
  };
};

const mergeProps = (stateToProps, dispatchToProps, ownProps) => {
  return Object.assign({}, stateToProps, ownProps, {
    routeChange: stateToProps.routeChange(dispatchToProps.dispatch)
  });
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(Home);
