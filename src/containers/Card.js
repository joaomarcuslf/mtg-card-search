import Card from "../components/Card";
import { connect } from "react-redux";

const mapStateToProps = (state, props) => {
  return Object.assign({}, props, {
    card: props.navigation.state.params.card
  });
};

export default connect(mapStateToProps)(Card);
