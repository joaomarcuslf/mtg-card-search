import CardView from "../components/CardView";
import { connect } from "react-redux";
import { NavigationActions } from "react-navigation";

const mapStateToProps = (state, props) => {
  return Object.assign(
    {
      routeChange: dispatch => (name, card) =>
        dispatch(
          NavigationActions.navigate({
            routeName: "Card",
            params: { name, card }
          })
        )
    },
    props
  );
};

const mapDispatchToProps = dispatch => ({ dispatch });

const mergeProps = (stateToProps, dispatchToProps, ownProps) => {
  return Object.assign({}, stateToProps, ownProps, {
    routeChange: stateToProps.routeChange(dispatchToProps.dispatch)
  });
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(
  CardView
);
