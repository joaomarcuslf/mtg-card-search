import React from "react";
import { BackHandler } from "react-native";
import { addNavigationHelpers } from "react-navigation";
import { createReduxBoundAddListener } from "react-navigation-redux-helpers";
import { Provider, connect } from "react-redux";
import store from "./src/store";
import AppNavigator from "./src/screens";

const addListener = createReduxBoundAddListener("root");

class App extends React.Component {
  componentWillMount() {
    BackHandler.addEventListener(
      "hardwareBackPress",
      (() => {
        const { dispatch, navigation } = this.props;
        if (navigation.routes.length === 1) {
          return false;
        }
        // if (shouldCloseApp(nav)) return false
        dispatch({ type: "Navigation/BACK" });
        return true;
      }).bind(this)
    );
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress");
  }

  render() {
    return (
      <AppNavigator
        navigation={addNavigationHelpers({
          dispatch: this.props.dispatch,
          state: this.props.navigation,
          addListener
        })}
      />
    );
  }
}

const mapStateToProps = state => ({
  navigation: state.navigation
});

const AppWithNavigationState = connect(mapStateToProps)(App);

export default class Root extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <AppWithNavigationState />
      </Provider>
    );
  }
}
